//
//  ViewController.swift
//  scoreKepperSwift
//
//  Created by Canyon Duncan on 9/12/17.
//  Copyright © 2017 Canyon Duncan. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {

    @IBAction func pinch(_ sender: UIPinchGestureRecognizer) {
        team1Text.text = "CANYON"
        team2Text.text = "DUNCAN"
    }
    @IBOutlet weak var team2Text: UITextField!
    @IBOutlet weak var team1Text: UITextField!
    @IBOutlet var team1Label: UILabel!
    @IBOutlet var team2Label: UILabel!
    @IBOutlet var stepper1Outlet: UIStepper!
    @IBOutlet var stepper2Outlet: UIStepper!
    
    @IBAction func team2Stepper(_ sender: UIStepper) {
        team2Label.text = String(format: "%.0f", stepper2Outlet.value)
        if(stepper2Outlet.value == stepper1Outlet.value){
            team2Label.textColor = UIColor.black
            team1Label.textColor = UIColor.black
        }
        else if(stepper2Outlet.value >= (stepper1Outlet.value + 1)){
            team2Label.textColor = UIColor.green
            team1Label.textColor = UIColor.red
        }
        else{
            team2Label.textColor = UIColor.red
            team1Label.textColor = UIColor.green
        }
        
        
        
        if(stepper2Outlet.value == 21){
            //let winner = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "winnerPopUp") as! popUpViewController
            //self.addChildViewController(winner)
            //winner.view.frame = self.view.frame
            //self.view.addSubview(winner.view)
            //winner.didMove(toParentViewController: self)
            
            performSegue(withIdentifier: "SecondVCSegue", sender: nil)
            resetButton(self)
        }
        
    }
    
    @IBAction func team1Stepper(_ sender: UIStepper) {
        team1Label.text = String(format:"%.0f", stepper1Outlet.value)
        if(stepper2Outlet.value == stepper1Outlet.value){
            team2Label.textColor = UIColor.black
            team1Label.textColor = UIColor.black
        }
        else if(stepper2Outlet.value >= (stepper1Outlet.value + 1)){
            team2Label.textColor = UIColor.green
            team1Label.textColor = UIColor.red
        }
        else{
            team2Label.textColor = UIColor.red
            team1Label.textColor = UIColor.green
        }
        
        if(stepper1Outlet.value == 21){
            //let winner = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "winnerPopUp") as! popUpViewController
            //self.addChildViewController(winner)
            //winner.view.frame = self.view.frame
            //self.view.addSubview(winner.view)
            //winner.didMove(toParentViewController: self)
            
            performSegue(withIdentifier: "SecondVCSegue", sender: nil)
            resetButton(self)
            
        }
    }
    
    
    @IBAction func resetButton(_ sender: Any) {
        team1Label.text = "0"
        team2Label.text = "0"
        stepper1Outlet.value = 0
        stepper2Outlet.value = 0
        team2Label.textColor = UIColor.black
        team1Label.textColor = UIColor.black
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.team1Text.delegate = self
        self.team2Text.delegate = self
        // Do any additional setup after loading the view, typically from a nib.
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        team1Text.resignFirstResponder()
        team2Text.resignFirstResponder()
        return true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "SecondVCSegue" {
            if let secondVC = segue.destination as? popUpViewController{
                if (stepper1Outlet.value >= stepper2Outlet.value){
                    secondVC.infoObject = team1Text.text
                }
                else{
                    secondVC.infoObject = team2Text.text
                }
                
            }
        }
    }
    
    
    
    

}

