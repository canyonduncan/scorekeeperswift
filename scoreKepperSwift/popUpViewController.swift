//
//  popUpViewController.swift
//  scoreKepperSwift
//
//  Created by Canyon Duncan on 9/14/17.
//  Copyright © 2017 Canyon Duncan. All rights reserved.
//

import UIKit

class popUpViewController: UIViewController {
    
    var infoObject:String?

    @IBOutlet weak var winnerLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        
        if infoObject != nil {
                winnerLabel.text = infoObject! + " WINS"
        }
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closePopUp(_ sender: Any) {
        self.view.removeFromSuperview()
        self.dismiss(animated: true)
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
